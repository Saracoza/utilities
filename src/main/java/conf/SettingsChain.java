package conf;

import java.io.IOException;
import java.util.Hashtable;

public class SettingsChain
{
    private Hashtable<String, String> settingsChain = new Hashtable<>();



    private SettingsChain() throws IOException
    {
        var fileReader = new java.io.FileReader(util.FilesManager.getInstance().getFilePath("settings.txt"));
        var buffReader = new java.io.BufferedReader(fileReader);

        String   lineBuffer;
        String[] optionAndValue;

        while ((lineBuffer = buffReader.readLine()) != null) {
            optionAndValue = lineBuffer.split("=");
            settingsChain.put(
                    optionAndValue[0],
                    optionAndValue[1]
            );
        }

        fileReader.close();
        buffReader.close();
    }



    private static SettingsChain instance = null;
    public  static SettingsChain createInstance() throws IOException
    {
        if (instance == null) {
            instance = new SettingsChain();
        }
        return instance;
    }
    public  static SettingsChain getInstance()
    {
        return instance;
    }



    public String getSettingValue(String settingName)
    {
        return settingsChain.get(settingName);
    }
}
