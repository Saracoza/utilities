package util;

import java.io.IOException;

public class FilesManager
{
    private FilesManager() {}
    private static FilesManager instance = null;
    public  static FilesManager getInstance()
    {
        if (instance == null) {
            instance = new FilesManager();
        }
        return instance;
    }



    public String getFilePath(String filename) throws IOException
    {
        var resource = getClass().getClassLoader().getResource(filename);
        if (resource == null) {
            throw new IOException("Can't obtain file path: " + filename);
        }
        else return resource.getPath().replace("%20", " ");
    }
}
