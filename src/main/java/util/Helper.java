package util;

import java.io.*;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class Helper
{
    private String helpText;



    private Helper(String helpTextFile) throws IOException
    {
        var helpTextStream = new FileInputStream(FilesManager.getInstance().getFilePath(helpTextFile));

        helpText = ansi().fg(YELLOW).a(new String(helpTextStream.readAllBytes())).reset().toString();

        helpTextStream.close();
    }



    private static Helper instance = null;
    public  static void createInstance(String helpTextFile) throws IOException
    {
        if (instance == null) {
            instance = new Helper(helpTextFile);
        }
    }
    public  static Helper getInstance()
    {
        return instance;
    }



    public void displayHelp()
    {
        System.out.println(helpText);
    }
}
