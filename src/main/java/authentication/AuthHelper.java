package authentication;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.zip.DataFormatException;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

public class AuthHelper
{
    public static byte[][] buildAuthObj()
    {
        byte[][] authObj = new byte[10][];

        var in = new java.util.Scanner(System.in);

        System.out.println(ansi().fg(CYAN).a("Please enter the following data needed for authentication:").reset());
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|      PNC      |-> ").reset()); authObj[0] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|   IC Series   |-> ").reset()); authObj[1] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|   IC Number   |-> ").reset()); authObj[2] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|   Issued by   |-> ").reset()); authObj[3] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|   Exp. Date   |-> ").reset()); authObj[4] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|    F. Name    |-> ").reset()); authObj[5] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|    L. Name    |-> ").reset()); authObj[6] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|   Birthdate   |-> ").reset()); authObj[7] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|      Sex      |-> ").reset()); authObj[8] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator() + "|  Citizenship  |-> ").reset()); authObj[9] = in.nextLine().getBytes();
        System.out.print(ansi().fg(CYAN).a("+---------------+" + System.lineSeparator()).reset());

        return authObj;
    }

    @SuppressWarnings("unchecked")
    public static String serializeAuthObj(byte[][] authObj)
    {
        JSONObject serializedAuthObj = new JSONObject();

        serializedAuthObj.put("PNC",         new String(authObj[0]));
        serializedAuthObj.put("SeriesIC",    new String(authObj[1]));
        serializedAuthObj.put("NumberIC",    new String(authObj[2]));
        serializedAuthObj.put("IssuedBy",    new String(authObj[3]));
        serializedAuthObj.put("ExpDate",     new String(authObj[4]));
        serializedAuthObj.put("NameF",       new String(authObj[5]));
        serializedAuthObj.put("NameL",       new String(authObj[6]));
        serializedAuthObj.put("BirthDate",   new String(authObj[7]));
        serializedAuthObj.put("Sex",         new String(authObj[8]));
        serializedAuthObj.put("Citizenship", new String(authObj[9]));

        return serializedAuthObj.toJSONString();
    }

    public static byte[][] deserializeAuthObj(String serializedAuthObj) throws DataFormatException
    {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(serializedAuthObj);

            byte[][] authObj = new byte[10][];

            authObj[0] = ((String) jsonObject.get("PNC"        )).getBytes();
            authObj[1] = ((String) jsonObject.get("SeriesIC"   )).getBytes();
            authObj[2] = ((String) jsonObject.get("NumberIC"   )).getBytes();
            authObj[3] = ((String) jsonObject.get("IssuedBy"   )).getBytes();
            authObj[4] = ((String) jsonObject.get("ExpDate"    )).getBytes();
            authObj[5] = ((String) jsonObject.get("NameF"      )).getBytes();
            authObj[6] = ((String) jsonObject.get("NameL"      )).getBytes();
            authObj[7] = ((String) jsonObject.get("BirthDate"  )).getBytes();
            authObj[8] = ((String) jsonObject.get("Sex"        )).getBytes();
            authObj[9] = ((String) jsonObject.get("Citizenship")).getBytes();

            return authObj;

        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }

    public static void clearAuthObj(byte[][] authObj)
    {
        if (authObj == null) return;

        for (byte[] field : authObj) java.util.Arrays.fill(field, (byte) 0);
    }
}
