package logging;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class LogManager
{
    private java.io.PrintWriter logWriter;



    private LogManager() throws IOException
    {
        logWriter = new java.io.PrintWriter(util.FilesManager.getInstance().getFilePath("log.log"));
    }



    private static LogManager instance = null;
    public  static void createInstance() throws IOException
    {
        if (instance == null) {
            instance = new LogManager();
        }
    }
    public  static LogManager getInstance()
    {
        return instance;
    }



    public synchronized void logOpenedConnection(String address, int port)
    {
        logWriter.println("[" + DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss:SSS").format(ZonedDateTime.now()) + "][" + address + " " + port + "][---]: Connection opened");
    }

    public synchronized void logClosedConnection(String address, int port)
    {
        logWriter.println("[" + DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss:SSS").format(ZonedDateTime.now()) + "][" + address + " " + port + "][---]: Connection closed");
    }

    public synchronized void logSentData(String address, int port, String data)
    {
        logWriter.println("[" + DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss:SSS").format(ZonedDateTime.now()) + "][" + address + " " + port + "][-->]: " + org.apache.commons.text.StringEscapeUtils.escapeJava(data));
    }

    public synchronized void logRecvData(String address, int port, String data)
    {
        logWriter.println("[" + DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss:SSS").format(ZonedDateTime.now()) + "][" + address + " " + port + "][<--]: " + org.apache.commons.text.StringEscapeUtils.escapeJava(data));
    }



    public void CloseLogFile()
    {
        logWriter.close();
    }
}