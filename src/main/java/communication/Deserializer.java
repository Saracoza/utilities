package communication;

import org.javatuples.Pair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.zip.DataFormatException;

public class Deserializer
{
    public static Pair<String, String> deserializeCommand(String serializedCommand) throws DataFormatException
    {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(serializedCommand);

            return new Pair<>(
                    (String) jsonObject.get("cmd"),
                    (String) jsonObject.get("arg")
            );
        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }

    public static Pair<ResponseType, String> deserializeResponse(String serializedResponse) throws DataFormatException
    {
        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(serializedResponse);

            return new Pair<>(
                    ResponseType.valueOf(
                            (String) jsonObject.get("type")
                    ),
                    (String) jsonObject.get("text")
            );
        } catch (Exception e) {
            throw new DataFormatException("Invalid data!");
        }
    }
}
