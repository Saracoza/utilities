package communication;

import org.json.simple.JSONObject;

public class Serializer
{
    @SuppressWarnings("unchecked")
    public static String serializeCommand(String cmd, String arg)
    {
        JSONObject serializedCommand = new JSONObject();

        serializedCommand.put("cmd", cmd);
        serializedCommand.put("arg", arg);

        return serializedCommand.toJSONString();
    }

    @SuppressWarnings("unchecked")
    public static String serializeResponse(ResponseType responseType, String text)
    {
        JSONObject serializedResponse = new JSONObject();

        var type = responseType.toString();
        serializedResponse.put("type", type);
        serializedResponse.put("text", text);

        return serializedResponse.toJSONString();
    }
}
